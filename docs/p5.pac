// PAC file for Firefox, Linux, etc.
function FindProxyForURL(url, host) {
  alert("URL: " + url);

  // .cms network
  if (shExpMatch(url,"*.cms:*") || shExpMatch(url,"*.cms/*")) {
    alert("CMS Private Network");
    return "SOCKS5 127.0.0.1:5000";
  }

  // All other requests go directly to the WWW:
  return "DIRECT";
}
