# VFAT

For an in-depth guide on the VFAT3 please consult the VFAT3 manual available [here](VFAT3_User_Manual_v2.3.pdf).
The VFAT3 is a much more complicated ASIC than the VFAT2 and requires a  little bit more knowledge to successfully use.
While the VFAT3 manual should serve as the end-all-be-all reference on the ASIC, a summary of some generally useful information is provided.

## General overview of the VFAT

The VFAT has three gain settings of its preamplifier (low, medium, high), two comparator modes ("arming", aka leading-edge, or CFD), several shaping times, two on-ASIC 10-bit ADC's for DAC monitoring, and an internal temperature sensor.

!!! important
    For the chip to function correctly and for all bias currents/voltages to be set properly, the `CFG_IREF` value must be set such that the reference current is 10 uA.
    The VFAT team has calibration each chip and determined this value, so all that is needed is to use the provided value.
    However, this value is unique per VFAT and care should be taken to ensure it is properly assigned.

## Chip ID

The VFAT also has a hardware e-fuse, which specifies the unique chip ID as a 32-bit integer.
In GE1/1 values 0 to 5000 are expected.

!!! note
    Early prototypes provided this value raw.
    Later it was determined that there was a significant risk of single bit flipping, and the chip ID was encoded before being fused.
    The encoding algorithm is a Reed-Muller (2,5) code, which provides the ability to detect and correct up to three bit flips, as well as detect, but not correct, four bit flips.
    All production VFATs have this value encoded, and the software performs the decoding internally, such that the only time this would be raised to the user level is if an error occurred due to more than three bit flips.

## Gain settings

You can change the gain settings of the preamp by writing the following set of registers:

=== "High"
    ``` sh
    CFG_RES_PRE = 1
    CFG_CAP_PRE = 0
    ```

=== "Medium"
    ``` sh
    CFG_RES_PRE = 2
    CFG_CAP_PRE = 1
    ```

=== "Low"
    ``` sh
    CFG_RES_PRE = 4
    CFG_CAP_PRE = 3
    ```

It is recommended to use the medium preamp gain setting as it was shown that the high gain setting causes strange behavior due to either saturation, after pulsing, or cross-talk.

## Comparator and shaper settings

To switch the comparator modes write the following set of registers:

=== "CFD mode"
    ``` sh
    CFG_SEL_COMP_MODE = 0
    CFG_FORCE_EN_ZCC = 0
    ```

=== "Arming mode (ARM)"
    ``` sh
    CFG_SEL_COMP_MODE = 1
    CFG_FORCE_EN_ZCC = 0
    ```

It is recommended to use the comparator in CFD mode.
If using the comparator in CFD mode, the shaping time should be set to the maximum to try to integrate the full pulse charge for the CFD technique.
If the comparator is used in arming mode, the shaping time should be set to the minimum to trigger the comparator as fast as possible (when pulse is over threshold).
These two can be accomplished via:

=== "CFD mode"
    ``` sh
    CFG_FP_FE = 0x7
    CFG_PT = 0xf
    ```

=== "Arming mode"
    ``` sh
    CFG_FP_FE = 0x0
    CFG_PT = 0x1
    ```

## Calibration modules settings

The calibration module can inject charge either in current injection of voltage step pulsing.
Both modes use the same circuit but are compliments of one and other (e.g., high current injection is low voltage step).
From S-curve results in the lab we have not see a difference between these two modes and voltage step pulsing is typically used by default.

Both the comparator and the calibration module can be configured to look at (inject) either positive or negative polarity pulses.
For calibration scans to be effective the polarity of the calibration module must match the polarity expected by the comparator.
Additionally during data taking the polarity the comparator should match the polarity of the GEM signal (i.e., negative polarity).
To ensure there are no mistakes both polarities are set such that:

``` sh
CFG_SEL_POL = 0x0
CFG_CAL_SEL_POL = 0x0
```

## DAC monitoring

To ensure proper temperature reading and any DAC monitoring the `CFG_VREF_ADC` must be set such that this is as close to 1.0V as possible (again provided by VFAT team at production time).
The `HV3b_V2` hybrids only have the internal temperature sensor on the VFAT ASIC while `HV3b_V3` and `V4` hybrids have an external PT100 sensor for monitoring temperature.

The comparator has two voltage DAC registers for specifying the voltage on the comparator (note this should *not* be confused with the threshold, which is the 50% point on an s-curve, i.e., where the channel responds to charge 50% of the time at fixed comparator voltage setting).
One value is the `CFG_THR_ARM_DAC` and the other is the `CFG_THR_ZCC_DAC`.
The later must be calibrated for proper channel trimming; the former is of little concern since this is not used in either arming or CFD comparator modes.
For more details on comparator voltage settings see the VFAT manual.

Finally there are a few calibration coefficients that are needed:

- `CAL_DACM`, slope in `y=mx+b` for converting `CFG_CAL_DAC` to `fC`,
- `CAL_DACB`, as `CAL_DACM` but for intercept,
- `ADC0M`, slope in `y=mx+b` for converting ADC counts to `mV` for ADC0,
- `ADC0B`, as `ADC0M` but for intercept,
- `ADC1M`, as `ADC0M` but for `ADC1`,
- `ADC1B`, as `ADC0B` but for `ADC1`,
- `CAL_THR_ARM_DAC_M`, slope in `y=mx+b` for converting
  `CFG_THR_ARM_DAC` to `fC` (needed for trimming),
- `CAL_THR_ARM_DAC_B`, as `CAL_THR_ARM_DAC_M` but for intercept.

## Checking VFAT synchronization

To check if the VFATs are synchronized on `OHY` from `gem_reg.py` execute:

``` console
kw SYNC_ERR_CNT Y
kw CFG_RUN Y
```

where `Y` is an integer representing the OH number.
Any VFAT whose sync error counter is non-zero indicates unstable communication.
In rare cases the sync error counters can all be `0x0` but communication may still not be good (i.e., you're on the edge of a good/bad phase for that GBT elink).
This is what the second keyword read (`kw`) is checking, it is a slow control command to the VFATs directly.
If `0xdeaddead` is returned for any `CFG_RUN` value, this indicates you do not have good communication.

Typical causes of bad communication are:

- The VFAT is not physically present on the hardware
- The GBT phase setting for that e-link is bad
- There is a problem with the hardware (VFAT, OH, or GEB)

## Checking VFAT registers

You can get information on `VFATY` of `OHX` from `gem_reg.py` by executing:

``` console
kw GEM_AMC.OH.OHX.GEB.VFATY.CFG_
read GEM_AMC.OH.OHX.GEB.VFATY.HW_CHIP_ID
```

This will print the values of all global registers for this VFAT.

Information about the channel register for channel `Z` can be obtained via:

``` console
kw GEM_AMC.OH.OHX.GEB.VFATY.VFAT_CHANNELS.CHANNELZ
```

## DAC Monitoring

The VFAT has two internal 10 bit SAR ADCs.
They each use two difference voltage references:

- `ADC0` uses the internal reference derived from the bandgap,
- `ADC1` uses an external reference tied to the input digital voltage (DVDD)

These can monitor the following values:

| Monitor Sel | State          | Register name (VFAT manual) | Bits     | Min | Max  | Register name (address table) | Note                                     |
|-------------|----------------|-----------------------------|----------|-----|------|-------------------------------|------------------------------------------|
| 1           | Calib IDC      | `GBL_CFG_CAL_0`             | \[9:2\]  | 0   | 0xff | `CFG_CAL_DAC`                 |                                          |
| 2           | Preamp InpTran | `GBL_CFG_BIAS_1`            | \[7:0\]  | 0   | 0xff | `CFG_BIAS_PRE_I_BIT`          |                                          |
| 3           | Pream LC       | `GBL_CFG_BIAS_2`            | \[13:8\] | 0   | 0x3f | `CFG_BIAS_PRE_I_BLCC`         |                                          |
| 4           | Preamp FC      | `GBL_CFG_BIAS_1`            | \[13:8\] | 0   | 0x3f | `CFG_BIAS_PRE_I_BSF`          |                                          |
| 5           | Shap FC        | `GBL_CFG_BIAS_3`            | \[15:8\] | 0   | 0xff | `CFG_BIAS_SH_I_BFCAS`         |                                          |
| 6           | Shap Inpair    | `GBL_CFG_BIAS_3`            | \[7:0\]  | 0   | 0xff | `CFG_BIAS_SH_I_BDIFF`         |                                          |
| 7           | SD Inpair      | `GBL_CFG_BIAS_4`            | \[7:0\]  | 0   | 0xff | `CFG_BIAS_SD_I_BDIFF`         |                                          |
| 8           | SD FC          | `GBL_CFG_BIAS_5`            | \[7:0\]  | 0   | 0xff | `CFG_BIAS_SD_I_BFCAS`         |                                          |
| 9           | SD SF          | `GBL_CFG_BIAS_5`            | \[13:8\] | 0   | 0x3f | `CFG_BIAS_SD_I_BSF`           |                                          |
| 10          | CFD Bias1      | `GBL_CFG_BIAS_0`            | \[5:0\]  | 0   | 0x3f | `CFG_BIAS_CFD_DAC_1`          |                                          |
| 11          | CFD Bias2      | `GBL_CFG_BIAS_0`            | \[11:6\] | 0   | 0x3f | `CFG_BIAS_CFD_DAC_2`          |                                          |
| 12          | CFD Hyst       | `GBL_CFG_HYS`               | \[5:0\]  | 0   | 0x3f | `CFG_HYST`                    |                                          |
| 13          | CFD  Ireflocal | N/A                         | N/A      | N/A | N/A  | N/A                           | Fixed value, no register                 |
| 14          | CFD ThArm      | `GBL_CFG_THR`               | \[7:0\]  | 0   | 0xff | `CFG_THR_ARM_DAC`             |                                          |
| 15          | CFD ThZcc      | `GBL_CFG_THR`               | \[15:8\] | 0   | 0xff | `CFG_THR_ZCC_DAC`             |                                          |
| 16          | SLVS Ibias     | `GBL_CFG_BIAS_6`            | \[11:6\] | 0   | 0xff | ?                             | Does not appear in Section 7.5 Registers |
| 32          | BGR            | N/A                         | N/A      | N/A | N/A  | N/A                           | Fixed value, no register                 |
| 33          | Calib Vstep    | `GBL_CFG_CAL_0`             | \[9:2\]  | 0   | 0xff | `CFG_CAL_DAC`                 |                                          |
| 34          | Preamp Vref    | `GBL_CFG_BIAS_2`            | \[7:0\]  | 0   | 0xff | `CFG_BIAS_PRE_VREF`           |                                          |
| 35          | Vth Arm        | `GBL_CFG_THR`               | \[7:0\]  | 0   | 0xff | `CFG_THR_ARM_DAC`             |                                          |
| 36          | Vth ZCC        | `GBL_CFG_THR`               | \[15:8\] | 0   | 0xff | `CFG_THR_ZCC_DAC`             |                                          |
| 37          | V Tsens Int    | N/A                         | N/A      | N/A | N/A  | N/A                           | Fixed value, no register                 |
| 38          | V Tsens Ext    | N/A                         | N/A      | N/A | N/A  | N/A                           | Fixed value, no register                 |
| 39          | ADC_Vref       | `GBL_CFG_CTR_4`             | \[9:8\]  | 0   | 0x3  | `CFG_ADC_VREF`                |                                          |
| 40          | ADC VinM       | N/A                         | N/A      | N/A | N/A  | N/A                           | Fixed value, no register                 |
| 41          | SLVS Vref      | `GBL_CFG_BIAS_6`            | \[5:0\]  | 0   | 0x3f | ?                             | Does not appear in Section 7.5 Registers |
